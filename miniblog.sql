-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2023 at 11:58 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miniblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `blogid` int(11) NOT NULL,
  `blog_title` varchar(255) DEFAULT NULL,
  `blog_desc` text DEFAULT NULL,
  `blog_image` varchar(300) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=>Published 0=>Unpublished',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Stores Blog Data';

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`blogid`, `blog_title`, `blog_desc`, `blog_image`, `status`, `created_at`, `updated_at`) VALUES
(3, 'imAGE', 'priyanka', 'assets/upload/blogimg/Priyanka.jpg', 1, '2023-04-01 13:50:03', '2023-04-01 13:50:03'),
(4, 'my blog', 'This is a premium blog respective to my last blog.please check.', 'assets/upload/blogimg/baby.jpg', 1, '2023-04-02 04:56:42', '2023-04-02 07:45:36'),
(6, 'new BLog', 'This is a different blog i am adding', 'assets/upload/blogimg/p31.png', 1, '2023-04-02 07:43:58', '2023-04-02 07:43:58'),
(7, 'My another Blog', 'I have changed image of my blog.', 'assets/upload/blogimg/boy1.jpg', 0, '2023-04-02 07:52:22', '2023-04-02 08:25:19'),
(8, 'Blog Test With CKeditor', '<h2>Updated my previous blog .with<span class=\"marker\"> CKEditor.</span></h2>\r\n', 'assets/upload/blogimg/p11.png', 0, '2023-04-02 09:22:58', '2023-04-02 09:43:19'),
(9, 'Codeigniter', '<p>Hello Everyone,</p>\r\n\r\n<h1>My new article.</h1>\r\n', 'assets/upload/blogimg/boy11.jpg', 1, '2023-04-02 12:15:33', '2023-04-02 12:17:15');

-- --------------------------------------------------------

--
-- Table structure for table `backendusers`
--

CREATE TABLE `backendusers` (
  `id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1 COMMENT '1=> Active 0=>Blocked'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Login for Backend Panel';

--
-- Dumping data for table `backendusers`
--

INSERT INTO `backendusers` (`id`, `username`, `password`, `status`) VALUES
(1, 'admin', '123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`blogid`);

--
-- Indexes for table `backendusers`
--
ALTER TABLE `backendusers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `blogid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `backendusers`
--
ALTER TABLE `backendusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
